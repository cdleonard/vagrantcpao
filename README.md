# Vagrant-based tests for linux TCP Authentication Option

This repo contains scripts to build and run linux patched with TCP-AO and run
tests inside vagrant. This is meant to be portable and have minimal requirements
for the host machine.

The gitlab-ci configuration is tied to details of a specific gitlab runner
through mounts and environment variables, mostly for speed.

## Quick Guide

`# apt-get install $(cat outer-apt-requirements.txt)`

`$ ./vagrantcpao.py --help`

It is possible to use normal vagrant commands such as `vagrant up` and `vagrant ssh`.

As part of initial setup a `vagrant.ssh_config` is generated, this can be used to run commands much faster:

`$ ssh -F vagrant.ssh_config default`

## Links

* Tests: https://github.com/cdleonard/tcp-authopt-test/
* Linux branch: https://github.com/cdleonard/linux/tree/tcp_authopt
