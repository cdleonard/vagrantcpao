# vi: set ft=ruby :

require 'etc'

vagrant_cwd = File.dirname(__FILE__)

Vagrant.configure("2") do |config|
  config.vm.box = "generic/ubuntu2004"
  config.vm.synced_folder ".", "/vagrant", type: "rsync"
  config.vm.provision :shell, path: "vagrant-provision.sh"
  config.vm.provision "ansible", run: "always" do |ansible|
    ansible.verbose = "v"
    ansible.become = true
    ansible.playbook = "ansible-playbook.yml"
    ansible.compatibility_mode = "2.0"
    ansible.extra_vars = {
      ansible_python_interpreter: "/usr/bin/python3"
    }
  end
  config.vm.provider "virtualbox" do |vbox|
    vbox.cpus = Etc.nprocessors / 2
    vbox.memory = 4096
    vbox.customize ["modifyvm", :id, "--uart1", "0x3f8", "4"]
    vbox.customize ["modifyvm", :id, "--uartmode1", "file", "#{vagrant_cwd}/console.log"]
    vbox.customize ["modifyvm", :id, "--nictype1", "virtio"]
    vbox.customize ["storagectl", :id, "--name", "IDE Controller", "--hostiocache", "off"]
  end
  config.vm.network :forwarded_port,
    id: 'ssh',
    guest: 22,
    host: 16719,
    auto_correct: true
end
