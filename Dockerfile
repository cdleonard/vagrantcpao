# Dockerfile used for gitlab image
FROM ubuntu:20.04

# APT
COPY apt-requirements.txt outer-apt-requirements.txt ./
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        $(cat apt-requirements.txt) \
        $(cat outer-apt-requirements.txt) \
    && \
    rm -rf /var/lib/apt/lists/*

# PIP
COPY setup.cfg setup.py ./
RUN python3 -m pip install -e .[test,hack]

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
