#! /bin/bash
#
# Build libyang
# Based on frr ubuntu20-ci Dockerfile
#

set -e -x

LIBYANG_SRC=~/libyang

if [[ ! -d $LIBYANG_SRC ]]; then
    git init $LIBYANG_SRC
    git -C $LIBYANG_SRC fetch https://github.com/CESNET/libyang.git --depth 1 tag v2.0.0
    git -C $LIBYANG_SRC checkout v2.0.0
fi

if [[ ! -f $LIBYANG_SRC/build/Makefile ]]; then
    cmake \
        -S "$LIBYANG_SRC" \
        -B "$LIBYANG_SRC/build" \
        -DCMAKE_INSTALL_PREFIX:PATH=/usr \
        -DCMAKE_BUILD_TYPE:String="Release"
fi

make -C "$LIBYANG_SRC/build" -j "$(nproc)"
sudo make -C "$LIBYANG_SRC/build" install
