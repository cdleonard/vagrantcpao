#! /bin/bash

set -e -x

FRR_SRC=~/frr
FRR_URL=https://github.com/FRRouting/frr.git
FRR_GIT_REF=master
if [[ ! -d $FRR_SRC ]]; then
    git init $FRR_SRC
    git -C $FRR_SRC fetch "$FRR_URL" --depth 1 "$FRR_GIT_REF"
    git -C $FRR_SRC checkout FETCH_HEAD
fi

cd ~/frr

if [[ ! -f ./configure ]]; then
    ./bootstrap.sh
fi

if [[ ! -f Makefile ]]; then
    ./configure \
        --prefix=/usr \
        --localstatedir=/var/run/frr \
        --sbindir=/usr/lib/frr \
        --sysconfdir=/etc/frr \
        --enable-user=frr \
        --enable-group=frr \
        --enable-vty-group=frrvty \
        --enable-vtysh \
        --disable-{ripd,ripngd,ospf6d,isisd,eigrpd,nhrpd,ldpd,babeld} \
        --enable-pim6d \
        --with-pkg-extra-version=-vagrantcpao
fi
make -j"$(nproc)"
sudo make install
