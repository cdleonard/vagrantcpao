#! /bin/bash

set -e -x
best_kver=$(dpkg-query -f '${db:Status-Abbrev}${Package}\n' --show 'linux-image-[456]*-generic'|grep ^ii|sort -V|tail -n1)
best_kver=${best_kver:15}
[[ -n $best_kver ]]
[[ -f /boot/vmlinuz-$best_kver ]]

advanced_label=$(sed /boot/grub/grub.cfg -ne 's/^.*\(gnulinux-advanced.*\)'\'' .*$/\1/p')
[[ -n $advanced_label ]]
specific_label=$(sed /boot/grub/grub.cfg -ne 's/^.*\(gnulinux-'"$best_kver"'.*\)'\'' .*$/\1/p')
[[ -n $specific_label ]]

cfg_file=/etc/default/grub.d/vagrantcpao-default.cfg
if grep "$specific_label" "$cfg_file"; then
    echo "Default grub up-to-date"
else
    echo "GRUB_DEFAULT='$advanced_label>$specific_label'" > "$cfg_file"
    update-grub
fi
exit 0
