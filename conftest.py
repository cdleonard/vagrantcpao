import logging

import pytest

from vagrantcpao import logging_config


def pytest_configure(config: pytest.Config):
    assert len(logging.root.handlers) == 0
    logging_config()
