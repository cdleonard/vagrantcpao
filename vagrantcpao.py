#! /usr/bin/env python3

import dataclasses
import logging
import os
import re
import shlex
import subprocess
import sys
import time
from enum import Enum
from pathlib import Path
from textwrap import dedent
from typing import List, Optional, Sequence, Tuple, Union

import click
import waiting

logger = logging.getLogger(__name__)


LOGGING_FORMAT = (
    "%(asctime)s: %(module)s:%(funcName)s:%(lineno)d: %(levelname)s: %(message)s"
)
LOGGING_TIME_FORMAT = "%H:%M:%S"
LOGGING_MSEC_FORMAT = "%s.%03d"


def logging_config():
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)
    fmt = logging.root.handlers[0].formatter
    assert fmt is not None
    fmt.default_time_format = LOGGING_TIME_FORMAT
    fmt.default_msec_format = LOGGING_MSEC_FORMAT


class RebootMode(Enum):
    DO = "do"
    NO = "no"
    Auto = "auto"


class LinuxKernelBuildMode(Enum):
    DO = "do"
    NO = "no"
    Auto = "auto"


class VagrantHaltMode(Enum):
    DO = "do"
    NO = "no"
    Auto = "auto"


@dataclasses.dataclass
class Opts:
    dry: bool = False
    vagrant_halt: VagrantHaltMode = VagrantHaltMode.Auto
    vagrant_destroy: bool = False
    vagrant_rsync: bool = False

    build_linux_inside_vagrant: bool = True
    linux_source_path: Optional[str] = None
    update_linux_source: bool = True
    menuconfig: bool = False
    clean_custom_kernels: bool = True
    linux_kernel_build_mode: LinuxKernelBuildMode = LinuxKernelBuildMode.Auto
    linux_kernel_git_url: str = "https://github.com/cdleonard/linux.git"
    linux_kernel_git_ref: str = "tcp_authopt"

    reboot_mode: RebootMode = RebootMode.Auto

    tcp_authopt_test_enabled: bool = True
    update_tcp_authopt_test: bool = True
    tcp_authopt_test_git_ref: str = "main"
    tcp_authopt_test_extra_args: Optional[str] = None
    tcp_authopt_test_extra_filter: List[str] = dataclasses.field(default_factory=list)

    kselftest_tcp_authopt_enabled: bool = False

    ssh_extra_args: Optional[str] = None

    fsck: bool = False


class EnumType(click.Choice):
    # https://github.com/pallets/click/issues/605#issuecomment-917022108

    def __init__(self, enum, case_sensitive=False):
        self._enum = enum
        choices = [item.value for item in list(enum)]
        super().__init__(choices=choices, case_sensitive=case_sensitive)

    def convert(self, value, param, ctx):
        if value is None or isinstance(value, Enum):
            return value

        converted_str = super().convert(value, param, ctx)
        return self._enum(converted_str)


@click.group(
    context_settings=dict(help_option_names=["-h", "--help"]),
    invoke_without_command=True,
)
@click.option(
    "-n",
    "--dry-run",
    "dry",
    is_flag=True,
    help="DRY RUN",
)
@click.option(
    "--ssh-extra-args",
    help="Extra arguments for ssh",
)
@click.option(
    "--do-vagrant-destroy/--no-vagrant-destroy",
    "vagrant_destroy",
    help="If enabled then ``vagrant destroy`` first",
    envvar="VAGRANT_DESTROY",
    default=False,
)
@click.option(
    "--do-fsck/--no-fsck",
    "fsck",
    help="If enabled then check filesystems first (before vagrant up)",
    default=False,
)
@click.option(
    "--do-vagrant-rsync/--no-vagrant-rsync",
    "vagrant_rsync",
    help="If enabled then run `vagrant rsync` first",
    default=False,
)
@click.option(
    "--menuconfig",
    is_flag=True,
    help="If enabled then run ``make menuconfig`` and update linux.config",
)
@click.option(
    "--build-linux-inside-vagrant",
    "build_linux_inside_vagrant",
    flag_value=True,
    default=True,
    help="Build linux inside vagrant",
)
@click.option(
    "--build-linux-outside-vagrant",
    "build_linux_inside_vagrant",
    flag_value=False,
    default=True,
    help="If set to true then build linux outside vagrant",
)
@click.option(
    "--linux-source-path",
    help="Path to linux source (default /home/vagrant/linux inside vagrant, otherwise must be specified)",
)
@click.option(
    "--do-update-linux-source",
    "update_linux_source",
    flag_value=True,
    default=True,
    help="Enable git update of linux source",
)
@click.option(
    "--no-update-linux-source",
    "update_linux_source",
    flag_value=False,
    default=True,
    help="Disable git update of linux source",
)
@click.option(
    "--linux-kernel-git-url",
    envvar="LINUX_KERNEL_GIT_URL",
    help="Git url to use for linux-kernel",
    default=Opts.linux_kernel_git_url,
)
@click.option(
    "--linux-kernel-git-ref",
    envvar="LINUX_KERNEL_GIT_REF",
    help="Git ref to use for linux-kernel",
    default=Opts.linux_kernel_git_ref,
)
@click.option(
    "--do-build-linux",
    "linux_kernel_build_mode",
    type=LinuxKernelBuildMode,
    flag_value=LinuxKernelBuildMode.DO,
    help="Build linux",
)
@click.option(
    "--no-build-linux",
    "linux_kernel_build_mode",
    type=LinuxKernelBuildMode,
    flag_value=LinuxKernelBuildMode.NO,
    help="Disable linux build",
)
@click.option(
    "--auto-build-linux",
    "linux_kernel_build_mode",
    type=LinuxKernelBuildMode,
    flag_value=LinuxKernelBuildMode.Auto,
    default=True,
    help="Build linux if not already running (default",
)
@click.option(
    "--do-reboot",
    "reboot_mode",
    type=RebootMode,
    flag_value=RebootMode.DO,
    help="Force reboot",
)
@click.option(
    "--no-reboot",
    "reboot_mode",
    type=RebootMode,
    flag_value=RebootMode.NO,
    help="Force skip reboot",
)
@click.option(
    "--auto-reboot",
    "reboot_mode",
    type=RebootMode,
    flag_value=RebootMode.Auto,
    default=True,
    help="Automatically reboot if built kernel is different from running kernel (default)",
)
@click.option(
    "--no-tcp-authopt-test",
    "tcp_authopt_test_enabled",
    flag_value=False,
    default=True,
    help="Skip everything related to tcp-authtop-test",
)
@click.option(
    "--do-tcp-authopt-test",
    "tcp_authopt_test_enabled",
    flag_value=True,
    default=True,
    help="Run tcp-authtop-test (default)",
)
@click.option(
    "--no-update-tcp-authopt-test",
    "update_tcp_authopt_test",
    flag_value=False,
    default=True,
    help="Disable git update of tcp-authopt-test source",
)
@click.option(
    "--tcp-authopt-test-git-ref",
    envvar="TCP_AUTHOPT_TEST_GIT_REF",
    default="main",
    help="Git ref to use for tcp_authopt_test",
)
@click.option(
    "-k",
    "--tcp-authopt-test-extra-filter",
    multiple=True,
    callback=lambda ctx, param, val: list(val),
    default=[],
    help="Extra pytest -k for tcp-authopt-test",
)
@click.option(
    "--tcp-authopt-test-extra-args",
    envvar="TCP_AUTHOPT_TEST_EXTRA_ARGS",
    help="Extra arguments for tcp-authopt-test",
)
@click.option(
    "--do-kselftest-tcp-authopt/--no-kselftest-tcp-authopt",
    "kselftest_tcp_authopt_enabled",
    help="Run the kselftest for tcp-authopt",
    default=False,
)
@click.option(
    "--auto-vagrant-halt",
    "vagrant_halt",
    type=VagrantHaltMode,
    flag_value=VagrantHaltMode.Auto,
    default=True,
    help="Only perform vagrant halt if it was not running before (default)",
)
@click.option(
    "--do-vagrant-halt",
    "vagrant_halt",
    type=VagrantHaltMode,
    flag_value=VagrantHaltMode.DO,
    help="Perform vagrant halt at the end",
)
@click.option(
    "--no-vagrant-halt",
    "vagrant_halt",
    type=VagrantHaltMode,
    flag_value=VagrantHaltMode.NO,
    help="Do not vagrant halt at the end",
)
@click.option(
    "--vagrant-halt",
    "vagrant_halt",
    envvar="VAGRANT_HALT",
    type=EnumType(VagrantHaltMode),
    default=VagrantHaltMode.Auto,
    help="Set vagrant halt mode",
)
@click.pass_context
def main(ctx: click.Context, **kw):
    """Run tests for Linux TCP Authentication Option using Vagrant

    This is a multi-step process, options are listed roughly in the order they
    take effect.
    """
    opts = Opts(**kw)
    ctx.obj = Tool(opts)
    if ctx.invoked_subcommand is None:
        click.echo(ctx.get_help(), color=ctx.color)
        ctx.exit()


@main.command(name="full-test", help="Run the full pipeline: Update build and test all")
@click.pass_obj
def click_main_full_test(tool: "Tool"):
    tool.main_full_test()


@main.command(name="help")
@click.pass_context
def click_help(ctx: click.Context):
    assert ctx.parent
    click.echo(ctx.parent.get_help(), color=ctx.color)
    ctx.exit()


@main.command(name="status")
@click.pass_obj
def click_status(tool: "Tool"):
    tool.main_print_status()


@main.command()
@click.pass_obj
def show_build_kernelrelease(tool: "Tool"):
    sys.stdout.write("{}\n".format(tool.get_build_kernelrelease()))


@main.command(help="Show kernelrelease list from base distro")
@click.pass_obj
def show_distro_kernelrelease(tool: "Tool"):
    sys.stdout.write("\n".join(tool.get_distro_kernelrelease_list()) + "\n")


@main.command()
@click.pass_obj
def show_vbox_id(tool: "Tool"):
    sys.stdout.write("{}\n".format(tool.get_vbox_id()))


@main.command()
@click.pass_obj
def show_disk_path(tool: "Tool"):
    sys.stdout.write("{}\n".format(tool.get_disk_path()))


@main.command(name="fsck", help="Halt and run fsck via qemu-nbd")
@click.pass_obj
def click_main_fsck(tool: "Tool"):
    tool.run_vagrant_halt()
    tool.fsck()


@main.command(name="start", help="Start vagrant and leave it running")
@click.pass_obj
def click_main_start(tool: "Tool"):
    tool.run_vagrant_up()
    tool.handle_vagrant_after_up()


@main.command(
    name="ensure-provision",
    help="Ensure provisioning is complete: check and reload if needed",
)
@click.pass_obj
def click_main_ensure_provision(tool: "Tool"):
    tool.ensure_provision()


@main.command(name="kexec-distro-kernel", help="Perform kexec into distro kernel")
@click.pass_obj
def click_main_kexec_distro_kernel(tool: "Tool"):
    with tool:
        distro_krel = tool.get_distro_kernelrelease()
        tool.kexec_reboot(distro_krel)
        live_krel = tool.get_live_kernelrelease()
        assert live_krel == distro_krel


@main.command(name="build-custom-kernel", help="Build and install custom kernel")
@click.pass_obj
def click_main_build_custom_kernel(tool: "Tool"):
    with tool:
        tool.do_linux_kernel_build()


@main.command(name="update-linux-source", help="Update linux kernel source")
@click.pass_obj
def click_main_update_linux_source(tool: "Tool"):
    with tool:
        tool.update_linux_source()


@main.command(name="kexec-custom-kernel", help="Perform kexec into custom kernel")
@click.pass_obj
def click_main_kexec_custom_kernel(tool: "Tool"):
    with tool:
        tool.do_kexec_custom_kernel()


@main.command(name="menuconfig", help="Run menuconfig and save")
@click.pass_obj
def click_main_menuconfig(tool: "Tool"):
    with tool:
        tool.do_menuconfig()


@main.command(name="reboot-test", help="Test kexec reboot in a loop")
@click.pass_obj
def click_main_reboot_test(tool: "Tool"):
    with tool:
        exitcode = tool.main_reboot_test()
    sys.exit(exitcode)


@main.command(name="console", help="Interact with VM console")
@click.pass_obj
def click_main_console(tool: "Tool"):
    import pyvboxcli

    vbid = tool.get_vbox_id()

    vb = pyvboxcli.VBoxManage()
    pyvboxcli.picocom_uart(vb, vbid, 1)


@main.command(name="reset", help="Run hard reset on VM")
@click.pass_obj
def click_main_reset(tool: "Tool"):
    tool.run_vagrant_subprocess(["halt", "--force"])
    tool.run_vagrant_up()
    tool.handle_vagrant_after_up()


@main.command(name="build-frr", help="Build FRR inside the VM")
@click.pass_obj
def click_build_frr(tool: "Tool"):
    with tool:
        tool.build_frr()


@main.command(
    name="topotests",
    help="run frr topotests",
    context_settings=dict(ignore_unknown_options=True),
)
@click.argument("argv", nargs=-1, type=click.UNPROCESSED)
@click.pass_obj
def click_topotests(tool: "Tool", argv):
    with tool:
        proc = tool.run_frr_topotests(argv)
        return proc.returncode


def parse_proc_uptime(arg: str) -> Tuple[float, float]:
    a, b = arg.split()
    return float(a), float(b)


def subprocess_args_to_string(args: Union[str, List[str]]) -> str:
    if isinstance(args, str):
        return args
    else:
        return shlex.join(args)


class Tool:
    opts = Opts()
    _vagrant_was_running: Optional[bool] = None
    vagrant_provision_check_timeout = 10
    vagrant_provision_timeout = 300

    @property
    def linux_source_path(self) -> str:
        if self.opts.linux_source_path:
            return self.opts.linux_source_path
        if self.opts.build_linux_inside_vagrant:
            return "/home/vagrant/linux"
        else:
            raise ValueError("Must specify --linux-source-path")

    linux_output_param = "build_vagrantcpao"
    """Value of O= argument"""

    @property
    def ssh_keepalive_args(self) -> List[str]:
        return [
            "-oServerAliveInterval=1",
            # Default ServerAliveCountMax=3
        ]

    @property
    def ssh_extra_args_list(self) -> List[str]:
        if self.opts.ssh_extra_args:
            return shlex.split(self.opts.ssh_extra_args)
        else:
            return []

    def __init__(self, opts: Optional[Opts] = None):
        if opts is None:
            opts = Opts()
        self.opts = opts

    @property
    def vagrant_ssh_config_path(self) -> str:
        """Get path to `vagrant ssh-config` output as a stirng"""
        return str(Path(__file__).parent / "vagrant.ssh_config")

    def subprocess_run(
        self,
        args: Union[str, Sequence[str]],
        **kwargs,
    ) -> subprocess.CompletedProcess:
        """Same as subprocess.run except returns an empty result in dry mode"""
        cmdstr = subprocess_args_to_string(args)  # type: ignore
        if self.opts.dry:
            logger.info("DRY: %s", cmdstr, stacklevel=2)
            return subprocess.CompletedProcess(args, returncode=0, stdout="", stderr="")
        else:
            logger.info("RUN: %s", cmdstr, stacklevel=2)
            proc = subprocess.run(args, **kwargs)
            return proc

    def update_ssh_config(self):
        """Update vagrant.ssh_config"""
        self.subprocess_run(
            "vagrant ssh-config > vagrant.ssh_config",
            check=True,
            shell=True,
            cwd=Path(__file__).parent,
        )

    def vagrant_ssh_run(
        self,
        script: str,
        user: Optional[str] = None,
        terminal=False,
        **kw,
    ):
        """Run a shell script inside the vagrant VM

        :param script: shell script to run
        :param user: user to run the script as (default to `vagrant`)
        :param kw: passed to subprocess.run
        """
        if self.opts.dry:
            logger.info("DRY: vagrant ssh -c %s", shlex.quote(script), stacklevel=2)
            return subprocess.CompletedProcess(
                script,
                returncode=0,
                stdout="",
                stderr="",
            )
        logger.info("RUN: vagrant ssh -c %s", shlex.quote(script), stacklevel=2)
        hostspec = "default"
        if user is not None:
            hostspec = user + "@" + hostspec
        inner = ["ssh", "-F", self.vagrant_ssh_config_path]
        inner += self.ssh_keepalive_args
        inner += self.ssh_extra_args_list
        if terminal:
            inner.append("-t")
        inner += [hostspec, script]
        return subprocess.run(inner, shell=False, **kw)

    def get_vagrant_cwd_path(self) -> Path:
        envval = os.getenv("VAGRANT_CWD")
        if envval is not None:
            return Path(envval)
        else:
            return Path(__file__).parent

    def get_vagrant_dotfile_path(self) -> Path:
        envval = os.getenv("VAGRANT_DOTFILE_PATH")
        if envval is not None:
            return Path(envval)
        else:
            return self.get_vagrant_cwd_path() / ".vagrant"

    def get_vbox_id(self) -> str:
        fn = self.get_vagrant_dotfile_path() / "machines/default/virtualbox/id"
        return fn.read_text()

    def get_disk_path(self) -> str:
        import pyvboxcli

        vb = pyvboxcli.VBoxManage()
        data = vb.get_vminfo_dict(self.get_vbox_id())
        disk_path = None
        for k, v in data.items():
            if re.match("(IDE|SATA) Controller-[0-9]-[0-9]", k) and v != "none":
                if disk_path is not None:
                    raise ValueError("Multiple disks found?")
                disk_path = v
        if disk_path is None:
            raise ValueError("No disk path found?")
        return disk_path

    def fsck(self) -> None:
        disk_path = self.get_disk_path()
        script = (
            f"""
set -e -x
# Outer disk image, parsed from vbox info
DISK={shlex.quote(disk_path)}
# Partition number is box-specific
PARTNO=3
"""
            + """

[ -b /dev/nbd0 ] || {
    echo >&2 "missing /dev/nbd0, is nbd module loaded?"
    exit 1
}

nbd_index_used() {
    nbd-client -c /dev/nbd${NBD_INDEX} >/dev/null
}

nbd_find_free_index() {
    for NBD_INDEX in `seq 0 15`; do
        if ! nbd_index_used ${NBD_INDEX}; then
            echo $NBD_INDEX
            return 0
        fi
    done
    echo >&2 "failed to find free nbd index"
    return 1
}

NBD_INDEX=$(nbd_find_free_index)
NBD=/dev/nbd${NBD_INDEX}

# connect
qemu-nbd -c "$NBD" "$DISK"

exit_trap() {
    qemu-nbd -d "$NBD"
}
trap exit_trap EXIT

# Create device nodes for partitions manually inside docker:
partprobe "$NBD"
if [[ ! -a "${NBD}p${PARTNO}" ]]; then
    NBD_MAJOR=43
    NBD_MINOR_HEX=$(stat -c%T "$NBD")
    mknod "${NBD}p${PARTNO}" b "$NBD_MAJOR" "$((0x$NBD_MINOR_HEX + $PARTNO))"
fi

# fsck returns non-zero on changes
fsck -fy "${NBD}p${PARTNO}" || true
sync
"""
        )
        subprocess.run(["sudo", "bash", "-c", script], check=True)

    def get_live_kernelrelease(self) -> str:
        """Get kernel version inside the vagrant VM via uname -r"""
        proc = self.vagrant_ssh_run(
            "uname -r",
            check=True,
            text=True,
            capture_output=True,
        )
        if self.opts.dry:
            return "LIVE_KERNELRELEASE"
        return proc.stdout.strip()

    def run_linux_build_script(
        self,
        script: str,
        *,
        check: bool = True,
        stdout: Union[None, int] = None,
    ) -> subprocess.CompletedProcess:
        """Run a shell script where linux is build"""
        if self.opts.build_linux_inside_vagrant:
            return self.vagrant_ssh_run(
                script,
                check=check,
                stdout=stdout,
                text=True,
            )
        else:
            return self.subprocess_run(
                script,
                shell=True,
                check=check,
                stdout=stdout,
                text=True,
            )

    def update_linux_source(self):
        script = dedent(
            f"""
                set -e -x

                LINUX_SOURCE={self.linux_source_path}
                LINUX_BRANCH={self.opts.linux_kernel_git_ref}
                LINUX_URL={self.opts.linux_kernel_git_url}

                if [[ ! -d $LINUX_SOURCE ]]; then
                    git clone \
                            --depth 100 \
                            --progress \
                            --verbose \
                            "$LINUX_URL" \
                            "$LINUX_SOURCE" \
                            --branch "$LINUX_BRANCH"
                else
                    git -C "$LINUX_SOURCE" fetch "$LINUX_URL" "$LINUX_BRANCH"
                    git -C "$LINUX_SOURCE" checkout -B "$LINUX_BRANCH" --force FETCH_HEAD
                fi
            """
        )
        self.run_linux_build_script(script)

    def scp_inner_to_outer(self, inner_path: str, outer_path: str):
        cmd = ["scp", "-F", self.vagrant_ssh_config_path]
        cmd += self.ssh_keepalive_args
        cmd += self.ssh_extra_args_list
        cmd += ["default:" + inner_path]
        cmd += [outer_path]
        return self.subprocess_run(cmd, shell=False, check=True)

    def scp_outer_to_inner(
        self,
        outer_path: str,
        inner_path: str,
        recursive: bool = False,
        root: bool = False,
    ):
        cmd = ["scp", "-F", self.vagrant_ssh_config_path]
        if recursive:
            cmd += ["-r"]
        cmd += self.ssh_keepalive_args
        cmd += self.ssh_extra_args_list
        cmd += [outer_path]
        if root:
            target = "root@default"
        else:
            target = "default"
        cmd += [target + ":" + inner_path]
        return self.subprocess_run(cmd, shell=False, check=True)

    def do_menuconfig(self):
        if not self.opts.build_linux_inside_vagrant:
            raise ValueError("Only supported inside vagrant")
        inner_dotconfig_path = (
            f"{self.linux_source_path}/{self.linux_output_param}/.config"
        )
        self.scp_outer_to_inner("./linux.config", inner_dotconfig_path)
        script = dedent(
            f"""
                set -e
                LINUX_SOURCE={shlex.quote(self.linux_source_path)}
                LINUX_OUTPUT={shlex.quote(self.linux_output_param)}
                make -C "$LINUX_SOURCE" O="$LINUX_OUTPUT" menuconfig
                make -C "$LINUX_SOURCE" O="$LINUX_OUTPUT" savedefconfig
                cp defconfig ~/linux.config
            """
        )
        self.vagrant_ssh_run(script, check=True, terminal=True)
        inner_defconfig_path = (
            f"{self.linux_source_path}/{self.linux_output_param}/defconfig"
        )
        self.scp_inner_to_outer(inner_defconfig_path, "./linux.config")

    def do_linux_kernel_build(self):
        """Build and install linux kernel"""
        script = dedent(
            f"""
                set -e -x

                LINUX_SOURCE={shlex.quote(self.linux_source_path)}
                LINUX_OUTPUT={shlex.quote(self.linux_output_param)}
                mkdir -p "$LINUX_SOURCE/$LINUX_OUTPUT"
            """
        )
        if self.opts.build_linux_inside_vagrant:
            script += 'cp /vagrant/linux.config "$LINUX_SOURCE/$LINUX_OUTPUT/.config"'
        else:
            linux_config_path = Path(__file__).parent / "linux.config"
            script += f'cp {shlex.quote(str(linux_config_path))} "$LINUX_SOURCE/$LINUX_OUTPUT/.config"'

        script += dedent(
            """
                export MAKEFLAGS="-j$(grep -c ^processor /proc/cpuinfo)"
                make -C "$LINUX_SOURCE" O="$LINUX_OUTPUT" olddefconfig
                make -C "$LINUX_SOURCE" O="$LINUX_OUTPUT"
            """
        )
        if self.opts.build_linux_inside_vagrant:
            script += dedent(
                """\
                    sudo make -C "$LINUX_SOURCE" \\
                            O="$LINUX_OUTPUT" \\
                            INSTALL_MOD_STRIP=1 \\
                            modules_install \\
                            install
                """
            )
        else:
            script += dedent(
                """\
                    make -C "$LINUX_SOURCE" \\
                        O="$LINUX_OUTPUT" \\
                        INSTALL_PATH="install" \\
                        INSTALL_MOD_PATH="install" \\
                        INSTALL_MOD_STRIP=1 \\
                        modules_install \\
                        install
                """
            )
        self.run_linux_build_script(script)
        if not self.opts.build_linux_inside_vagrant:
            output_path = f"{self.linux_source_path}/{self.linux_output_param}"
            install_path = f"{output_path}/install"
            krel = self.get_build_kernelrelease()
            local_modules_path = install_path + "/lib/modules/" + krel
            remote_modules_path = "/lib/modules/" + krel
            self.vagrant_ssh_run(
                f"mkdir -p {shlex.quote(remote_modules_path)}",
                user="root",
                check=True,
            )
            script = dedent(
                f"""
                    scp -F {shlex.quote(self.vagrant_ssh_config_path)} -r \\
                        {shlex.quote(local_modules_path)}/kernel \\
                        {shlex.quote(local_modules_path)}/modules.* \\
                        root@default:{shlex.quote(remote_modules_path)}
                """
            )
            self.subprocess_run(script, check=True, shell=True)
            image_name = "arch/x86/boot/bzImage"
            self.scp_outer_to_inner(
                f"{output_path}/{image_name}",
                f"/boot/vmlinuz-{krel}.tmp",
                root=True,
            )
            self.scp_outer_to_inner(
                f"{output_path}/System.map",
                f"/boot/System.map-{krel}.tmp",
                root=True,
            )
            cmd = [
                "installkernel",
                krel,
                f"/boot/vmlinuz-{krel}.tmp",
                f"/boot/System.map-{krel}.tmp",
            ]
            self.vagrant_ssh_run(shlex.join(cmd), user="root", check=True)

    def get_build_kernelrelease(self) -> str:
        """Get kernel version from make -s kernelrelease"""
        script = f"make -C {self.linux_source_path} O={self.linux_output_param} -s kernelrelease"
        proc = self.run_linux_build_script(script, stdout=subprocess.PIPE)
        if self.opts.dry:
            return "BUILD_KERNELRELEASE"
        return proc.stdout.rstrip()

    def get_distro_kernelrelease_list(self) -> List[str]:
        """Get list of recent kernel version via dpkg"""
        script = " dpkg-query -f '${db:Status-Abbrev}${Package}\n' --show 'linux-image-[456]*-generic'|grep ^ii|sort -V"
        proc = self.vagrant_ssh_run(
            script,
            check=True,
            stdout=subprocess.PIPE,
            text=True,
        )
        return [item[15:].strip() for item in proc.stdout.splitlines()]

    def get_distro_kernelrelease(self) -> str:
        """Get most recent kernel version via dpkg"""
        return self.get_distro_kernelrelease_list()[-1]

    def try_get_build_kernelrelease(self) -> Optional[str]:
        try:
            return self.get_build_kernelrelease()
        except subprocess.CalledProcessError:
            logger.debug("Failed make -s kernelrelease")
            return None

    def should_do_linux_kernel_build(self) -> bool:
        if self.opts.linux_kernel_build_mode == LinuxKernelBuildMode.DO:
            return True
        if self.opts.linux_kernel_build_mode == LinuxKernelBuildMode.NO:
            return False
        build_krel = self.try_get_build_kernelrelease()
        live_krel = self.get_live_kernelrelease()
        if build_krel == live_krel:
            logger.info("No need for linux build already running %s", live_krel)
            return False
        else:
            return True

    def handle_linux_kernel_build(self):
        if self.should_do_linux_kernel_build():
            self.do_linux_kernel_build()

    def try_get_uptime(self) -> Optional[float]:
        """Get uptime of vagrant guest or "None" on failure (during reboot)"""
        try:
            proc = self.vagrant_ssh_run(
                "cat /proc/uptime",
                stdout=subprocess.PIPE,
                stdin=subprocess.DEVNULL,
                check=True,
                text=True,
                timeout=5,
            )
        except subprocess.SubprocessError:
            # This catches both CalledProcessError and TimeoutError
            logger.info("Failed to read new uptime, maybe still booting")
            return None
        if self.opts.dry:
            return None
        return parse_proc_uptime(proc.stdout)[0]

    def kexec_reboot(self, kernelrelease: str):
        logger.info("kexec into %s", kernelrelease)
        uptime_margin = 5.0
        kexec_load_cmd = [
            "kexec",
            "--load",
            f"/boot/vmlinuz-{kernelrelease}",
            "--reuse-cmdline",
            "--initrd",
            f"/boot/initrd.img-{kernelrelease}",
        ]
        self.vagrant_ssh_run(shlex.join(kexec_load_cmd), user="root", check=True)
        self.vagrant_ssh_run("sync", user="root", check=True)
        kexec_exec_cmd = ["ssh", "-F", self.vagrant_ssh_config_path]
        kexec_exec_cmd += self.ssh_keepalive_args
        kexec_exec_cmd += self.ssh_extra_args_list
        kexec_exec_cmd += ["root@default", "kexec", "--exec"]
        if self.opts.dry:
            logger.info("DRY: %s", subprocess_args_to_string(kexec_exec_cmd))
            return
        else:
            logger.info("RUN: %s", subprocess_args_to_string(kexec_exec_cmd))
        old_local_time = time.monotonic()
        old_uptime = self.try_get_uptime()
        if old_uptime is None:
            raise Exception("Failed to parse current uptime")
        logger.debug("old_uptime=%.3f old_local_time=%.3f", old_uptime, old_local_time)
        kexec_popen = subprocess.Popen(kexec_exec_cmd)

        def check() -> bool:
            new_local_time = time.monotonic()
            new_uptime = self.try_get_uptime()
            if new_uptime is None:
                return False
            assert old_uptime is not None
            expected_old_uptime = old_uptime - old_local_time + new_local_time
            logger.debug(
                "new_uptime=%.3f versus expected_old_uptime=%.3f margin=%.3f new_local_time=%.3f",
                old_uptime,
                expected_old_uptime,
                uptime_margin,
                old_local_time,
            )
            if new_uptime < expected_old_uptime - uptime_margin:
                # reboot detected!
                return True

            # Poll after check so that an error is raised only if old uptime is
            # reported *AFTER* kexec script returns
            if kexec_popen.returncode is not None:
                msg = (
                    "Failed: kexec returned %d and new_uptime=%.3f >= expected_old_uptime=%.3f"
                    " with margin=%.3f from old_uptime=%.3f new_local_time=%.3f old_local_time=%.3f"
                ) % (
                    kexec_popen.returncode,
                    new_uptime,
                    expected_old_uptime,
                    uptime_margin,
                    old_uptime,
                    new_local_time,
                    old_local_time,
                )
                logger.error(msg)
                raise Exception(msg)
            kexec_popen.poll()

            logger.debug("uptime increasing but kexec did not return, try again")
            return False

        try:
            waiting.wait(check, timeout_seconds=60)
            logger.info("kexec successful")
        finally:
            kexec_popen.kill()

    def handle_reboot(self):
        if self.opts.reboot_mode == RebootMode.NO:
            return
        build_krel = self.get_build_kernelrelease()
        old_live_krel = self.get_live_kernelrelease()
        if self.opts.reboot_mode == RebootMode.Auto and build_krel == old_live_krel:
            logger.info("Already running %s", build_krel)
            return
        self.do_kexec_custom_kernel()

    def do_kexec_custom_kernel(self):
        build_krel = self.get_build_kernelrelease()
        self.kexec_reboot(build_krel)
        new_live_krel = self.get_live_kernelrelease()
        if not self.opts.dry and build_krel != new_live_krel:
            raise ValueError(f"Expected {build_krel} got {new_live_krel}")

    def update_tcp_authopt_test(self):
        script = dedent(
            f"""
                set -e -x

                SOURCE=/home/vagrant/tcp-authopt-test
                REF={self.opts.tcp_authopt_test_git_ref}
                URL=https://github.com/cdleonard/tcp-authopt-test.git

                if [[ ! -d "$SOURCE/.git" ]]; then
                    git init "$SOURCE"
                fi
                git -C "$SOURCE" fetch --progress --verbose "$URL" "$REF"
                git -C "$SOURCE" checkout --force FETCH_HEAD
            """
        )
        self.vagrant_ssh_run(script, check=True)

    def run_tcp_authopt_test(self):
        if self.opts.update_tcp_authopt_test:
            self.update_tcp_authopt_test()
        cmd = "/home/vagrant/tcp-authopt-test/run.sh"
        for arg in self.opts.tcp_authopt_test_extra_filter:
            cmd += " -k " + shlex.quote(arg)
        if self.opts.tcp_authopt_test_extra_args:
            cmd += " " + self.opts.tcp_authopt_test_extra_args
        self.vagrant_ssh_run(cmd, user="root", check=True)

    def run_kselftest_tcp_authopt(self):
        if not self.opts.build_linux_inside_vagrant:
            raise ValueError("Only supported when building linux inside vagrant")

        # Install khdr and build nettest alone out of selftests/net
        linux_output_path = os.path.join(
            self.linux_source_path, self.linux_output_param
        )
        bcmd = dedent(
            f"""
                make -C {self.linux_source_path} O={self.linux_output_param} headers
                export KHDR_INCLUDES="-isystem {linux_output_path}/usr/include"
                mkdir -p "{linux_output_path}/kselftest/net"
                make \
                    -C {self.linux_source_path} \
                    -C tools/testing/selftests/net \
                    O={self.linux_output_param} \
                    OUTPUT={linux_output_path}/kselftest/net \
                    {linux_output_path}/kselftest/net/nettest
            """
        )
        self.vagrant_ssh_run(bcmd, check=True)

        # Run `fcnal-test.sh -t tcp_authopt` with a hacked PATH
        rcmd = dedent(
            f"""
                export PATH={linux_output_path}/kselftest/net:$PATH
                {self.linux_source_path}/tools/testing/selftests/net/fcnal-test.sh -t tcp_authopt
            """
        )
        self.vagrant_ssh_run(rcmd, user="root", check=True)

    def build_frr(self):
        self.vagrant_ssh_run("/vagrant/build-libyang.sh", check=True)
        self.vagrant_ssh_run("/vagrant/build-frr.sh", check=True)

    def run_frr_topotests(self, argv: Sequence[str], **kw):
        cmd = f"cd /home/vagrant/frr/tests/topotests; pytest {shlex.join(argv)}"
        return self.vagrant_ssh_run(cmd, user="root", **kw)

    def main_with_vagrant(self):
        """Main part of the code executing with `vagrant up`"""
        if self.opts.update_linux_source:
            self.update_linux_source()
        if self.opts.menuconfig:
            self.do_menuconfig()
        if self.opts.clean_custom_kernels:
            self.vagrant_ssh_run(
                "/vagrant/clean-custom-kernels.sh", user="root", check=True
            )
        self.handle_linux_kernel_build()
        self.handle_reboot()
        if self.opts.tcp_authopt_test_enabled:
            self.run_tcp_authopt_test()
        if self.opts.kselftest_tcp_authopt_enabled:
            self.run_kselftest_tcp_authopt()

    def run_provision_check(self, check=False) -> bool:
        proc = self.vagrant_ssh_run(
            "/vagrant/vagrant-provision-check.sh",
            timeout=self.vagrant_provision_check_timeout,
            check=check,
        )
        return proc.returncode == 0

    def ensure_provision(self):
        if self.run_provision_check() == False:
            logger.info(
                "Vagrant provision check failed, doing `vagrant reload --provision`"
            )
            self.run_vagrant_subprocess(
                ["reload", "--provision"],
                timeout=self.vagrant_provision_timeout,
            )
            self.update_ssh_config()
            self.run_provision_check(check=True)
        else:
            logger.info("Verified provision")

    def main_full_test(self):
        for k in dataclasses.fields(self.opts):
            logger.info("opts.%s=%s", k.name, getattr(self.opts, k.name))
        if self.opts.vagrant_destroy:
            self.run_vagrant_destroy()
        if self.opts.fsck:
            self.run_vagrant_halt()
            self.fsck()
        with self:
            self.main_with_vagrant()

    def run_vagrant_subprocess(
        self,
        argv: List[str],
        check: bool = True,
        timeout: Optional[float] = None,
        capture_stdout: bool = False,
    ) -> subprocess.CompletedProcess:
        return self.subprocess_run(
            ["vagrant"] + argv,
            check=check,
            timeout=timeout,
            text=True,
            cwd=Path(__file__).parent,
            stdout=subprocess.PIPE if capture_stdout else None,
        )

    def run_vagrant_up(self):
        self.run_vagrant_subprocess(["up"])

    def run_vagrant_halt(self):
        self.run_vagrant_subprocess(["halt"])

    def run_vagrant_destroy(self):
        self.run_vagrant_subprocess(["destroy", "--force"])

    def vagrant_status_running(self) -> bool:
        proc = self.run_vagrant_subprocess(
            ["status", "--machine-readable"],
            capture_stdout=True,
            check=False,
        )
        if proc.returncode:
            logger.error("Failed to determine vagrant status:\n%s", proc.stdout)
            proc.check_returncode()
        if self.opts.dry:
            return False
        return "default,state,running\n" in proc.stdout

    def handle_vagrant_up(self):
        """Perform `vagrant up` and remember _vagrant_was_running"""
        if self.opts.vagrant_halt == VagrantHaltMode.Auto:
            assert self._vagrant_was_running is None
            if self.vagrant_status_running():
                self._vagrant_was_running = True
            else:
                self._vagrant_was_running = False
                self.run_vagrant_up()
        else:
            self.run_vagrant_up()

    def handle_vagrant_halt(self):
        """Perform `vagrant halt` if required based on VagrantHaltMode"""
        if self.opts.vagrant_halt == VagrantHaltMode.Auto:
            assert self._vagrant_was_running is not None
            if not self._vagrant_was_running:
                self.run_vagrant_halt()
            self._vagrant_was_running = None
        elif self.opts.vagrant_halt == VagrantHaltMode.DO:
            self.run_vagrant_halt()

    def handle_vagrant_after_up(self):
        self.update_ssh_config()
        if self.opts.vagrant_rsync:
            self.run_vagrant_subprocess(["rsync"])
        self.ensure_provision()

    def __enter__(self):
        self.handle_vagrant_up()
        self.handle_vagrant_after_up()
        return self

    def __exit__(self, *a):
        self.handle_vagrant_halt()

    def main_reboot_test(self, loop_count=100, continue_on_failure=False) -> int:
        krel_list = self.get_distro_kernelrelease_list()
        if len(krel_list) < 2:
            raise ValueError("Need two distro kernel releases")
        kr1 = krel_list[-1]
        kr2 = krel_list[-2]
        fail_count = 0

        for loop_iter in range(1, loop_count + 1):
            kr = kr1 if loop_iter % 2 else kr2
            logger.info("ITER %d: kexec reboot into %s", loop_iter, kr)
            logger.info("STAT FAIL %d DONE %d", fail_count, loop_iter - 1)
            try:
                self.kexec_reboot(kr)
            except:
                if not continue_on_failure:
                    raise
                logger.error("failed reboot, fsck and try again", exc_info=True)
                self.run_vagrant_halt()
                self.fsck()
                self.run_vagrant_up()
                fail_count += 1
                continue

            live_krel = self.get_live_kernelrelease()
            assert live_krel == kr
        return 0 if fail_count == 0 else 1

    def main_print_status(self):
        if not self.vagrant_status_running():
            logger.info("Vagrant is not running")
            return
        logger.info("Vagrant is running")
        logger.info("Live kernelrelease is %s", self.get_live_kernelrelease())
        logger.info("Build kernelrelease is %s", self.get_build_kernelrelease())
        logger.info("Distro kernelrelease is %s", self.get_distro_kernelrelease())


if __name__ == "__main__":
    logging_config()
    main()
