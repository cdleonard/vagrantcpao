#! /bin/bash

set -e -x
if ! getent group frr; then
    groupadd -r -g 92 frr
fi
if ! getent group frrvty; then
    groupadd -r -g 85 frrvty
fi
if ! getent passwd frr; then
    adduser \
        --system \
        --ingroup frr \
        --home /home/frr \
        --gecos "FRR suite" \
        --shell /bin/bash \
        frr
fi
usermod -a -G frrvty frr
echo 'frr ALL = NOPASSWD: ALL' | tee /etc/sudoers.d/frr
mkdir -p /home/frr
chown frr.frr /home/frr
