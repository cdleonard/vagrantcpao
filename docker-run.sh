#! /bin/bash
#
# Run inside docker like on gitlab
#

set -e -x

docker build .
docker run \
    --interactive \
    --tty \
    --privileged \
    --volume "$(pwd):/app" \
    --workdir /app \
    "$(docker build -q .)" \
    "$@"
