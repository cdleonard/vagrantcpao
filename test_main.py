import dataclasses
import logging
import os
import shlex
from contextlib import ExitStack
from pathlib import Path
from typing import Dict, List
from unittest import mock

import click
import pytest
import yaml
from click.testing import CliRunner, Result
from pytest import LogCaptureFixture

from vagrantcpao import LinuxKernelBuildMode, Opts, RebootMode, Tool, VagrantHaltMode
from vagrantcpao import main as main_command

logger = logging.getLogger(__name__)


def get_null_env_override(cmd: click.Command):
    env: Dict[str, None] = dict()
    for param in cmd.params:
        if param.envvar:
            assert isinstance(param.envvar, str)
            env[param.envvar] = None
    return env


def click_invoke_opts(*a, **kw) -> Opts:
    """Invoke the main command and return the Opts"""
    runner = CliRunner()
    assert isinstance(main_command, click.Command)
    # By default ignore all variables in the environment of pytest
    assert "env" not in kw
    kw["env"] = get_null_env_override(main_command)
    with mock.patch("vagrantcpao.Tool") as mock_tool_class:
        result = runner.invoke(main_command, *a, **kw)
        assert result.exception is None
        assert result.exit_code == 0
        assert mock_tool_class.called
        opts = mock_tool_class.call_args[0][0]
        assert isinstance(opts, Opts)
        return opts


def click_invoke(argv: List[str]) -> Result:
    """Invoke the main command and return the click.test.Result"""
    runner = CliRunner()
    result = runner.invoke(
        main_command,
        argv,
        env=get_null_env_override(main_command),
        catch_exceptions=False,
    )
    return result


def test_parse_vagrant_halt():
    opts = click_invoke_opts(["--no-vagrant-halt"])
    assert opts.vagrant_halt == VagrantHaltMode.NO


def test_default_menuconfig():
    opts = click_invoke_opts([])
    assert opts.menuconfig == False
    opts = click_invoke_opts(["--menuconfig"])
    assert opts.menuconfig == True


def test_default():
    """Check the default on Opts are accurate"""
    class_defaults = Opts()
    click_defaults = click_invoke_opts([])
    for f in dataclasses.fields(Opts):
        click_val = getattr(click_defaults, f.name)
        class_val = getattr(class_defaults, f.name)
        assert click_val == class_val, f"field {f.name}"


def test_parse_build_linux():
    opts = click_invoke_opts(["--no-build-linux"])
    assert opts.linux_kernel_build_mode == LinuxKernelBuildMode.NO


def test_parse_reboot():
    opts = click_invoke_opts([])
    assert isinstance(opts.reboot_mode, RebootMode)
    assert opts.reboot_mode == RebootMode.Auto
    opts = click_invoke_opts(["--no-reboot"])
    assert isinstance(opts.reboot_mode, RebootMode)
    assert opts.reboot_mode == RebootMode.NO
    opts = click_invoke_opts(["--do-reboot"])
    assert opts.reboot_mode == RebootMode.DO
    opts = click_invoke_opts(["--do-reboot"])
    assert opts.reboot_mode == RebootMode.DO
    opts = click_invoke_opts(["--do-reboot", "--no-reboot"])
    assert opts.reboot_mode == RebootMode.NO
    opts = click_invoke_opts(["--no-reboot", "--do-reboot"])
    assert opts.reboot_mode == RebootMode.DO


def test_parse_misc():
    opts = click_invoke_opts(["--no-update-tcp-authopt-test"])
    assert opts.update_tcp_authopt_test == False
    opts = click_invoke_opts(["--no-update-linux-source"])
    assert opts.update_linux_source == False
    opts = click_invoke_opts(["--do-update-linux-source"])
    assert opts.update_linux_source == True


def test_parse_fsck():
    assert click_invoke_opts([]).fsck == False
    assert click_invoke_opts(["--do-fsck"]).fsck == True
    assert click_invoke_opts(["--no-fsck"]).fsck == False


def test_parse_tcp_authopt_test_enabled():
    opts = click_invoke_opts(["--no-tcp-authopt-test"])
    assert opts.tcp_authopt_test_enabled == False
    opts = click_invoke_opts(["--do-tcp-authopt-test"])
    assert opts.tcp_authopt_test_enabled == True
    assert opts.tcp_authopt_test_enabled == True


def test_mock_subprocess():
    with mock.patch("vagrantcpao.subprocess") as subprocess:
        Tool(Opts(reboot_mode=RebootMode.NO)).main_full_test()
        assert subprocess.run.called


def test_dry():
    with mock.patch("vagrantcpao.subprocess") as subprocess:
        Tool(Opts(dry=True)).main_full_test()
        assert not subprocess.run.called
        assert not subprocess.Popen.called


def test_dry_outer_linux_build():
    with mock.patch("vagrantcpao.subprocess") as subprocess:
        opts = Opts(
            dry=True,
            build_linux_inside_vagrant=False,
            linux_source_path="./fake-linux-source",
        )
        Tool(opts).main_full_test()
        assert not subprocess.run.called
        assert not subprocess.Popen.called


def test_dry_reboot(caplog: pytest.LogCaptureFixture):
    with caplog.at_level(logging.INFO):
        with mock.patch("vagrantcpao.subprocess") as subprocess:
            Tool(Opts(dry=True, reboot_mode=RebootMode.DO)).main_full_test()
            assert not subprocess.run.called
            assert not subprocess.Popen.called
    assert caplog.text.count("kexec --load") == 1


def test_extra_args(caplog: pytest.LogCaptureFixture):
    with caplog.at_level(logging.INFO):
        Tool(Opts(dry=True, tcp_authopt_test_extra_args="-k md5")).main_full_test()
    assert caplog.text.count("run.sh -k md5") == 1


def test_extra_args_filter(caplog: pytest.LogCaptureFixture):
    with caplog.at_level(logging.INFO):
        Tool(Opts(dry=True, tcp_authopt_test_extra_filter=["md5"])).main_full_test()
    assert caplog.text.count("run.sh -k md5") == 1

    opts = click_invoke_opts(["-k", "md5"])
    assert opts.tcp_authopt_test_extra_filter == ["md5"]


def test_gitlab_defaults():
    """Compare envvar between click and .gitlab-ci.yaml"""
    gitlab_yaml_path = Path(__file__).parent / ".gitlab-ci.yml"
    assert gitlab_yaml_path.exists()
    with open(gitlab_yaml_path, "r") as f:
        gitlab_yaml = yaml.safe_load(f)
    assert isinstance(gitlab_yaml["variables"], dict)
    for click_param in main_command.params:
        envvar = click_param.envvar
        if envvar == None:
            continue
        gitlab_var = gitlab_yaml["variables"].get(click_param.envvar)
        if gitlab_var == None:
            logger.info("click envvar %s not exposed in gitlab", envvar)
            continue
        assert isinstance(gitlab_var, dict)
        if gitlab_var["value"] != click_param.default:
            logger.warning(
                "%s had different default in gitlab %r and click %r",
                envvar,
                gitlab_var["value"],
                click_param.default,
            )
        if not isinstance(click_param, click.Option):
            raise ValueError("click param %r is not an Option", click_param)
        click_help = click_param.help
        if gitlab_var["description"] != click_help:
            logger.info(
                "%s had different description in gitlab %r and click %r",
                envvar,
                gitlab_var["description"],
                click_help,
            )


def test_subcommand():
    runner = CliRunner()
    # By default ignore all variables in the environment of pytest
    with mock.patch("vagrantcpao.Tool") as mock_tool_class:
        mock_tool = mock_tool_class.return_value
        mock_tool.get_build_kernelrelease.return_value = "v1.2.3"
        result = runner.invoke(
            main_command,
            ["show-build-kernelrelease"],
            env=get_null_env_override(main_command),
        )
        assert result.exit_code == 0
        assert mock_tool_class.called
        assert mock_tool.get_build_kernelrelease.called
        assert "v1.2.3" in result.stdout


@pytest.fixture(scope="session")
def running_tool():
    """Return a tool instance which is in the running state"""
    with Tool() as tool:
        yield tool


def test_get_distro_kernelrelease(running_tool: Tool):
    kr = running_tool.get_distro_kernelrelease()
    script = f"[ -f /boot/vmlinuz-{shlex.quote(kr)} ]"
    running_tool.vagrant_ssh_run(script, check=True)


def test_vbox_id(running_tool: Tool):
    assert running_tool.get_vbox_id()


def test_disk_path(running_tool: Tool):
    assert os.path.exists(running_tool.get_disk_path())


def test_reboot(running_tool: Tool):
    exitcode = running_tool.main_reboot_test(2, continue_on_failure=False)
    assert exitcode == 0


@pytest.mark.parametrize("running_before", [True, False])
def test_vagrant_halt_mode(running_before: bool):
    tool = Tool()
    assert tool.opts.vagrant_halt == VagrantHaltMode.Auto
    with ExitStack() as exit_stack:
        mock_vagrant_up = exit_stack.enter_context(
            mock.patch.object(tool, "run_vagrant_up")
        )
        mock_vagrant_halt = exit_stack.enter_context(
            mock.patch.object(tool, "run_vagrant_halt")
        )
        exit_stack.enter_context(
            mock.patch.object(
                tool, "vagrant_status_running", return_value=running_before
            )
        )
        assert tool.vagrant_status_running() == running_before
        tool.handle_vagrant_up()
        assert mock_vagrant_up.called == (not running_before)
        tool.handle_vagrant_halt()
        assert mock_vagrant_halt.called == (not running_before)


def test_dry_build_custom_kernel(caplog: LogCaptureFixture):
    with caplog.at_level(logging.INFO):
        click_invoke(["--dry-run", "build-custom-kernel"])
    assert "modules_install" in caplog.text


def test_dry_update_linux_source(caplog: LogCaptureFixture):
    with caplog.at_level(logging.INFO):
        click_invoke(["--dry-run", "update-linux-source"])
    assert "git clone" in caplog.text


def test_frr_topotests_collect(running_tool):
    running_tool.build_frr()
    proc = running_tool.run_frr_topotests(
        ["--collectonly"],
        capture_output=True,
        text=True,
    )
    assert "tests collected" in proc.stdout


def test_help_subcommand():
    r = click_invoke(["help"])
    assert "kexec-custom-kernel" in r.output


def test_status_subcommand(running_tool: Tool):
    running_tool.main_print_status()
