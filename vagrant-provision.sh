#! /bin/bash

set -e -x

export DEBIAN_FRONTEND=noninteractive
mapfile -t apt_reqs < <(cat /vagrant/{frr-,}apt-requirements.txt)
if ! apt-get install --assume-yes --no-install-recommends --no-download "${apt_reqs[@]}"; then
    apt-get update
    apt-get install --assume-yes --no-install-recommends "${apt_reqs[@]}"
fi

# Allow ssh as root
cp -R /home/vagrant/.ssh /root/

# Cloudflare DNS is more reliable
sed -ie 's/addresses: \[4.2.2..*$/addresses: [1.1.1.1]/' /etc/netplan/01-netcfg.yaml
sudo netplan apply

/vagrant/vagrant-provision-frr-user.sh
python3 -m pip install -r /vagrant/frr-pip-requirements.txt

/vagrant/vagrant-provision-default-distro-kernel.sh
