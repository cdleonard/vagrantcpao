#! /bin/bash

set -e

print_help()
{
    cat >&2 <<MSG
linux-build-test.sh: Build linux in fragile configs related to tcp_authopt
==========================================================================

Builds the following configs:

    tcp_authopt_ipv6_mod tcp_authopt_off_md5_on tcp_authopt_on_md5_off tcp_authopt_off_md5_off; do

This can be quite slow but is the reliable way to ensure build doesn't break
MSG
}

LINUX_SOURCE=${LINUX_SOURCE:-/home/vagrant/linux}
if [[ -z $LINUX_CONFIG_BASE ]]; then
    LINUX_CONFIG_BASE=$(dirname "${BASH_SOURCE[0]}")/linux.config
fi

if [[ $# != 0 ]]; then
    print_help
    exit 2
fi
set -x

build_one_config()
{
    mkdir -p "$LINUX_SOURCE/build_$build_name"
    cp "$LINUX_CONFIG_BASE" "$LINUX_SOURCE/build_$build_name/.config"
    for conf in "${setup_vals[@]}"; do
        echo "$conf" >> "$LINUX_SOURCE/build_$build_name/.config"
    done
    make -C "$LINUX_SOURCE" O="build_$build_name" olddefconfig
    for conf in "${check_vals[@]}"; do
        grep -q "$conf" "$LINUX_SOURCE/build_$build_name/.config"
    done
    make -C "$LINUX_SOURCE" O="build_$build_name" -k || fail_count=$((fail_count+1))
}

fail_count=0

build_tcp_authopt_on_md5_off()
{
    build_name=tcp_authopt_on_md5_off
    setup_vals=(CONFIG_TCP_AUTHOPT=y CONFIG_TCP_MD5SIG=n)
    check_vals=(CONFIG_TCP_AUTHOPT=y 'CONFIG_TCP_MD5SIG is not set')
    build_one_config
}

build_tcp_authopt_off_md5_on()
{
    build_name=tcp_authopt_off_md5_on
    setup_vals=(CONFIG_TCP_AUTHOPT=n CONFIG_TCP_MD5SIG=y)
    check_vals=('CONFIG_TCP_AUTHOPT is not set' CONFIG_TCP_MD5SIG=y)
    build_one_config
}

build_tcp_authopt_off_md5_off()
{
    build_name=tcp_authopt_off_md5_off
    setup_vals=(CONFIG_TCP_AUTHOPT=n CONFIG_TCP_MD5SIG=n)
    check_vals=('CONFIG_TCP_AUTHOPT is not set' 'CONFIG_TCP_MD5SIG is not set')
    build_one_config
}

build_tcp_authopt_on_md5_off
build_tcp_authopt_off_md5_on
build_tcp_authopt_off_md5_off

if [[ $fail_count != 0 ]]; then
    echo "FAIL: $fail_count builds failed"
    exit 1
fi
exit 0
