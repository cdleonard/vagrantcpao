#! /bin/bash
#
# Check vagrant-provision.sh took full effect
# In particular changes to kernel cmdline need a reboot (or vagrant reload) to enforce.
#

set -e -x

mapfile -t apt_reqs < <(cat /vagrant/{frr-,}apt-requirements.txt)
if sudo apt-get install --assume-yes --no-install-recommends --no-download "${apt_reqs[@]}"; then
    echo "ok - apt requirements satisfied"
else
    echo "not ok - apt not requirements satisfied"
    exit 1
fi

[[ -f /etc/default/grub.d/vagrantcpao.cfg ]]
grep "debug ignore_loglevel systemd.log_level=info" /proc/cmdline

# only informative:
cat /sys/bus/clocksource/devices/clocksource0/current_clocksource
cat /sys/bus/clocksource/devices/clocksource0/available_clocksource

current_clocksource=$(cat /sys/bus/clocksource/devices/clocksource0/current_clocksource)
if [[ ! $current_clocksource == acpi_pm ]]; then
    echo "not ok - current_clocksource is not acpi_pm"
    exit 1
fi

if ! grep -q "tsc=unstable" /proc/cmdline; then
    echo "not ok - missing tsc=unstable"
    exit 1
fi

# check using cloudflare DNS
if resolvectl dns | grep -q 1.1.1.1; then
    echo "ok cloudflare dns"
else
    echo "not ok - wrong DNS"
    resolvectl dns
    exit 1
fi

if getent passwd frr; then
    echo "ok - has frr user"
else
    echo "not ok - missing frr user"
    exit 1
fi

if command -v pytest >/dev/null; then
    echo "ok - has pytest"
else
    echo "not ok - missing pytest"
    exit 1
fi

/vagrant/vagrant-provision-default-distro-kernel.sh
